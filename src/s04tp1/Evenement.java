package s04tp1;
import java.time.LocalDate;

public class Evenement {
	private String intitule;
	private String lieu;
	private LocalDate debut;
	private LocalDate fin;
	
	public Evenement(LocalDate debut, LocalDate fin){
		this.debut = debut;
		this.fin = fin;
		if(debut.isAfter(fin)){
			this.fin = debut;
		}
	}
	
	public String getIntitule() {
		return intitule;
	}



	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}



	public String getLieu() {
		return lieu;
	}



	public void setLieu(String lieu) {
		this.lieu = lieu;
	}



	public LocalDate getDebut() {
		return debut;
	}



	public void setDebut(LocalDate debut) {
		this.debut = debut;
	}



	public LocalDate getFin() {
		return fin;
	}



	public void setFin(LocalDate fin) {
		this.fin = fin;
	}



	public String toString(){
		return "S'intitule : "+this.intitule+" | a pour lieu : "+this.lieu+" | Date de d�but : "+debut+" | Date de fin :"+fin+"\n";
	}
	
	boolean equals(Evenement evenement){
		if(this.intitule.equals(evenement.intitule) && this.lieu.equals(evenement.lieu) && this.debut.equals(evenement.debut) && this.fin.equals(evenement.fin)){
			return true;
		}
		return false;
	}
	
	boolean chevauche(Evenement evenement){
		if(this.fin.isAfter(evenement.debut) && (!this.debut.isAfter(evenement.debut))){
			return true;
		}else if((!this.fin.isAfter(evenement.debut)) && this.debut.isAfter(evenement.debut)){
			return true;
		}
		return false;
	}
	
	
	
}
