package s04tp1;

import java.time.LocalDate;

public class EssaiAgenda {

	public static void main(String[] args) {
		Agenda e = new Agenda();
		Evenement evenement1 = new Evenement(LocalDate.of(1998, 8, 25), LocalDate.of(1998, 8, 26));
		Evenement evenement2 = new Evenement(LocalDate.of(1998, 8, 27), LocalDate.of(1998, 8, 28));
		Evenement evenement3 = new Evenement(LocalDate.of(1998, 8, 28), LocalDate.of(1998, 8, 27));
		Evenement evenement4 = new Evenement(LocalDate.of(1998, 8, 27), LocalDate.of(1998, 8, 30));
		Evenement evenement5 = new Evenement(LocalDate.of(1998, 8, 25), LocalDate.of(1998, 8, 27));
		Evenement evenement6 = new Evenement(LocalDate.of(1998, 8, 24), LocalDate.of(1998, 8, 26));
		Evenement evenement7 = new Evenement(LocalDate.of(1998, 9, 29), LocalDate.of(1998, 9, 30));
		Evenement evenement8 = new Evenement(LocalDate.of(1998, 8, 28), LocalDate.of(1998, 8, 29));
		
		e.entrerEvenement(evenement1);
		evenement1.setIntitule("Mon 1er �v�nement");
		evenement1.setLieu("Lille");
		
		e.entrerEvenement(evenement2);
		evenement2.setIntitule("Mon 2�me �v�nement");
		evenement2.setLieu("Paris");
		
		e.entrerEvenement(evenement3);
		evenement3.setIntitule("Mon 3�me �v�nement");
		evenement3.setLieu("Marseille");
		e.supprimerEvenement(2);
		
		e.entrerEvenement(evenement4);
		evenement4.setIntitule("Mon 4�me �v�nement");
		evenement4.setLieu("OnVerra");
		
		e.entrerEvenement(evenement5);
		evenement5.setIntitule("Mon 5�me �v�nement");
		evenement5.setLieu("OnVerra2");
		
		e.entrerEvenement(evenement6);
		evenement6.setIntitule("Mon 6�me �v�nement");
		evenement6.setLieu("OnVerra3");
		
		e.entrerEvenement(evenement7);
		evenement7.setIntitule("Mon 7�me �v�nement");
		evenement7.setLieu("OnVerra7");
		
		e.entrerEvenement(evenement8);
		evenement8.setIntitule("Mon 8�me �v�nement");
		evenement8.setLieu("Lyon");
		
		e.supprimerChevauchants(evenement1);
		
		//Du 27 au 30 ao�t
		e.supprimerEntre(evenement2, evenement4);
		
		System.out.println(e);

	}

}
