package s04tp1;

public class EssaiJeuDeDomino {

	public static void main(String[] args) {
		JeuDeDomino jeu = new JeuDeDomino();
		
		jeu.add(4, 6);
		jeu.add(1, 1);
		jeu.add(5, 6);
		jeu.add(4, 6);
		
		System.out.println("Jeu initial : "+jeu);
		
		jeu.remove(5, 6);
		
		System.out.println("Jeu apr�s suppression de 5 | 6 : "+jeu);
		
		JeuDeDomino jeu2 = new JeuDeDomino();
		
		jeu2.add(1, 1);
		jeu2.add(2, 4);
		jeu2.add(6, 3);
		
		System.out.println("Jeu2 sans union : "+jeu2);
		
		jeu.union(jeu2);
		
		System.out.println("Jeu2 apr�s union : "+jeu2);
	}

}
